#!/bin/bash

echo "Setting KUBECONFIG"

export  KUBECONFIG=$KUBECONFIG:/home/config

echo "Target cluster"

kubectl cluster-info

cd cfgsvr-src

echo "Deleting deployment if exists"

kubectl delete -f ConfigServer.yaml -n dev

sleep 5

echo "deploying"

kubectl create -f ConfigServer.yaml -n dev

echo "deployment success!"
