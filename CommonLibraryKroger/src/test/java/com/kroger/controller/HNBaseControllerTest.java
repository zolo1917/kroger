/*
 * Copyright (C) {year} VMware, LLC
 * https://VMware.com
 * helpnowplus-product-dev@vmware.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kroger.controller;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.junit4.SpringRunner;

import com.kroger.controller.KrogerBaseController;
import com.kroger.exception.KrogApplicationException;
import com.kroger.util.JWTTokenUtil;

@RunWith(SpringRunner.class)
public class HNBaseControllerTest {

	public static Logger logger = LoggerFactory.getLogger(HNBaseControllerTest.class);

	@Mock
	public JWTTokenUtil jwtTokenUtil;

	@InjectMocks
	KrogerBaseController hnbaseController;

	HashMap<String, Object> mapMock;

	@Before
	public void setUp() {
	}

	@Test
	public void testValidateUserAccessNRoles() throws KrogApplicationException {
		mapMock = new HashMap<>();
		mapMock.put("userName", "mock");
		mapMock.put("client_id", "client_id");
		when(jwtTokenUtil.getJWTPayloadFromAccessToken(isA(String.class))).thenReturn(mapMock);
		assertNotNull(hnbaseController.validateUserAccessNRoles(StringUtils.EMPTY, "mock", StringUtils.EMPTY));
	}

	@Test(expected = KrogApplicationException.class)
	public void testValidateSameImpersonateUserAccessNRoles() throws KrogApplicationException {
		mapMock = new HashMap<>();
		mapMock.put("userName", "mock");
		mapMock.put("client_id", "client_id");
		when(jwtTokenUtil.getJWTPayloadFromAccessToken(isA(String.class))).thenReturn(mapMock);
		assertNotNull(hnbaseController.validateUserAccessNRoles(StringUtils.EMPTY, "mock", "mock"));
	}

	@Test(expected = KrogApplicationException.class)
	public void testDifferentXRemoteUser() throws KrogApplicationException {
		mapMock = new HashMap<>();
		mapMock.put("userName", "mockInvalid");
		mapMock.put("client_id", "client_id");
		when(jwtTokenUtil.getJWTPayloadFromAccessToken(isA(String.class))).thenReturn(mapMock);
		hnbaseController.validateUserAccessNRoles(StringUtils.EMPTY, "mock", StringUtils.EMPTY);
	}

	@Test(expected = KrogApplicationException.class)
	public void testInvalidJWTPayload() throws KrogApplicationException {
		assertNotNull(hnbaseController.validateUserAccessNRoles(StringUtils.EMPTY, "mock", StringUtils.EMPTY));
	}

	@Test(expected = KrogApplicationException.class)
	public void testInvalidXRemoteUser() throws KrogApplicationException {
		mapMock = new HashMap<>();
		mapMock.put("userName", "mock");
		mapMock.put("client_id", "client_id");
		when(jwtTokenUtil.getJWTPayloadFromAccessToken(isA(String.class))).thenReturn(mapMock);
		assertNotNull(
				hnbaseController.validateUserAccessNRoles(StringUtils.EMPTY, "mapMock@mock.com", StringUtils.EMPTY));
	}

	@Test(expected = KrogApplicationException.class)
	public void testEmptyXRemoteUser() throws KrogApplicationException {
		mapMock = new HashMap<>();
		mapMock.put("userName", "mock");
		mapMock.put("client_id", "client_id");
		when(jwtTokenUtil.getJWTPayloadFromAccessToken(isA(String.class))).thenReturn(mapMock);
		assertNotNull(
				hnbaseController.validateUserAccessNRoles(StringUtils.EMPTY, StringUtils.EMPTY, StringUtils.EMPTY));
	}

	@Test
	public void testImpersonateValidateUserAccessNRoles() throws KrogApplicationException {
		mapMock = new HashMap<>();
		mapMock.put("userName", "mock");
		mapMock.put("client_id", "client_id");
		List<String> rolesList = new ArrayList<>();
		rolesList.add("Impersonator");
		mapMock.put("authorities", rolesList);
		when(jwtTokenUtil.getJWTPayloadFromAccessToken(isA(String.class))).thenReturn(mapMock);
		when(jwtTokenUtil.validateUserWithAccessToken(anyMap(), isA(String.class))).thenReturn(true);
		assertNotNull(hnbaseController.validateUserAccessNRoles(StringUtils.EMPTY, "mockUser", "mock"));
	}

	@Test(expected = KrogApplicationException.class)
	public void testInvalidUserRoles() throws KrogApplicationException {
		mapMock = new HashMap<>();
		mapMock.put("userName", "mock");
		mapMock.put("client_id", "client_id");
		List<String> rolesList = new ArrayList<>();
		rolesList.add("NotImpersonator");
		mapMock.put("authorities", rolesList);
		when(jwtTokenUtil.getJWTPayloadFromAccessToken(isA(String.class))).thenReturn(mapMock);
		when(jwtTokenUtil.validateUserWithAccessToken(anyMap(), isA(String.class))).thenReturn(true);
		assertNotNull(hnbaseController.validateUserAccessNRoles(StringUtils.EMPTY, "mockUser", "mock"));
	}

	@Test(expected = KrogApplicationException.class)
	public void testEmptyUserRoles() throws KrogApplicationException {
		mapMock = new HashMap<>();
		mapMock.put("userName", "mock");
		mapMock.put("client_id", "client_id");
		mapMock.put("authorities", null);
		when(jwtTokenUtil.getJWTPayloadFromAccessToken(isA(String.class))).thenReturn(mapMock);
		when(jwtTokenUtil.validateUserWithAccessToken(anyMap(), isA(String.class))).thenReturn(true);
		assertNotNull(hnbaseController.validateUserAccessNRoles(StringUtils.EMPTY, "mockUser", "mock"));
	}

	@Test(expected = KrogApplicationException.class)
	public void testInvalidAccessTokenMap() throws KrogApplicationException {
		mapMock = new HashMap<>();
		mapMock.put("userName", "mock");
		mapMock.put("client_id", "client_id");
		when(jwtTokenUtil.getJWTPayloadFromAccessToken(isA(String.class))).thenReturn(mapMock);
		when(jwtTokenUtil.validateUserWithAccessToken(anyMap(), isA(String.class))).thenReturn(true);
		assertNotNull(hnbaseController.validateUserAccessNRoles(StringUtils.EMPTY, "mockUser", "mock"));
	}

	@Test(expected = KrogApplicationException.class)
	public void testInvalidAccessToken() throws KrogApplicationException {
		mapMock = new HashMap<>();
		mapMock.put("userName", "mock");
		mapMock.put("client_id", "client_id");
		when(jwtTokenUtil.getJWTPayloadFromAccessToken(isA(String.class))).thenReturn(mapMock);
		when(jwtTokenUtil.validateUserWithAccessToken(anyMap(), isA(String.class))).thenReturn(false);
		assertNotNull(hnbaseController.validateUserAccessNRoles(StringUtils.EMPTY, "mockUser", "mock"));
	}
}
