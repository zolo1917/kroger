/*
 * Copyright (C) {year} VMware, LLC
 * https://VMware.com
 * helpnowplus-product-dev@vmware.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 

package com.kroger.exception.handler;

import static org.junit.Assert.assertEquals;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.beans.TypeMismatchException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.orm.jpa.JpaSystemException;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;

import com.kroger.exception.BadDataException;
import com.kroger.exception.ConfigException;
import com.kroger.exception.KrogApplicationException;
import com.kroger.exception.UserInActiveException;
import com.kroger.exception.UserNotFoundException;
import com.kroger.exception.UserUnauthorizedException;
import com.kroger.exception.handler.BaseExceptionHandler;
import com.kroger.util.CommonConstants;
import com.kroger.util.ServiceErrorCode;

@RunWith(SpringRunner.class)
public class BaseExceptionHandlerTest {

	@Spy
	private BaseExceptionHandler baseExceptionHandlerMock;

	@Mock
	JpaSystemException jpaSystemExceptionMock;

	@Mock
	MethodArgumentNotValidException methodArgumentNotValidExceptionMock;

	@Mock
	HttpMessageNotReadableException httpMessageNotReadableExceptionMock;

	@Mock
	TypeMismatchException typeMismatchExceptionMock;

	@Mock
	HttpRequestMethodNotSupportedException httpRequestMethodNotSupportedExceptionMock;

	@Mock
	BindingResult bindingResult;

	@Mock
	HttpServletRequest request;

	*//**
	 *//*
	@Test
	public void testHandleBadDataException() {
		assertEquals(HttpStatus.BAD_REQUEST,
				baseExceptionHandlerMock.handleBadDataException(
						new BadDataException(StringUtils.EMPTY, StringUtils.EMPTY, ServiceErrorCode.GENERIC_ERROR),
						request).getStatusCode());

	}

	*//**
	 * 
	 *//*
	@Test
	public void testHandleUserUnauthorizedException() {
		assertEquals(HttpStatus.UNAUTHORIZED, baseExceptionHandlerMock.handleUserUnauthorizedException(
				new UserUnauthorizedException(StringUtils.EMPTY, StringUtils.EMPTY, ServiceErrorCode.GENERIC_ERROR),
				request).getStatusCode());
	}

	@Test
	public void testHandleUserNotFoundException() {
		assertEquals(HttpStatus.NOT_FOUND,
				baseExceptionHandlerMock.handleUserNotFoundException(
						new UserNotFoundException(StringUtils.EMPTY, StringUtils.EMPTY, ServiceErrorCode.GENERIC_ERROR),
						request).getStatusCode());
	}

	@Test
	public void testHandleUserInActiveException() {
		assertEquals(HttpStatus.BAD_REQUEST,
				baseExceptionHandlerMock.handleUserInActiveException(
						new UserInActiveException(StringUtils.EMPTY, StringUtils.EMPTY, ServiceErrorCode.GENERIC_ERROR),
						request).getStatusCode());
	}

	@Test
	public void testHandleDataIntegrityViolationException() {
		assertEquals(HttpStatus.BAD_REQUEST, baseExceptionHandlerMock
				.handleDataIntegrityViolationException(new DataIntegrityViolationException(StringUtils.EMPTY), request)
				.getStatusCode());
	}

	@Test
	public void testHandleJpaSystemException() {
		Mockito.doReturn(new Exception()).when(jpaSystemExceptionMock).getMostSpecificCause();
		assertEquals(HttpStatus.BAD_REQUEST,
				baseExceptionHandlerMock.handleJpaSystemException(jpaSystemExceptionMock, request).getStatusCode());
	}

	@Test
	public void testHandleIllegalArgumentException() {
		assertEquals(HttpStatus.BAD_REQUEST,
				baseExceptionHandlerMock
						.handleIllegalArgumentException(new IllegalArgumentException(StringUtils.EMPTY), request)
						.getStatusCode());
	}

	@Test
	public void testHandleInvalidDataAccessApiUsageException() {
		assertEquals(HttpStatus.BAD_REQUEST, baseExceptionHandlerMock.handleInvalidDataAccessApiUsageException(
				new InvalidDataAccessApiUsageException(StringUtils.EMPTY), request).getStatusCode());
	}

	@Test
	public void testHandleHNApplicationException() {
		assertEquals(HttpStatus.BAD_REQUEST,
				baseExceptionHandlerMock
						.handleHNApplicationException(
								new KrogApplicationException(StringUtils.EMPTY, CommonConstants.BAD_REQUEST_ERROR_CODE,
										ServiceErrorCode.ACCESS_TOKEN_INVALID_ERROR.getErrorCode()),
								request)
						.getStatusCode());
	}

	@Test
	public void testHandleConfigException() {
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR,
				baseExceptionHandlerMock.handleConfigException(
						new ConfigException(StringUtils.EMPTY, StringUtils.EMPTY, ServiceErrorCode.GENERIC_ERROR),
						request).getStatusCode());
	}

	@Test
	public void testHandleAllExceptions() {
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, baseExceptionHandlerMock
				.handleAllExceptions(new Exception(StringUtils.EMPTY), request).getStatusCode());
	}

	@Test
	public void testHandleMethodArgumentNotValid() {
		FieldError f = new FieldError(StringUtils.EMPTY, StringUtils.EMPTY, StringUtils.EMPTY);
		Mockito.doReturn(bindingResult).when(methodArgumentNotValidExceptionMock).getBindingResult();
		Mockito.doReturn(f).when(bindingResult).getFieldError();
		assertEquals(HttpStatus.BAD_REQUEST, baseExceptionHandlerMock
				.handleMethodArgumentNotValid(methodArgumentNotValidExceptionMock, null, null, null).getStatusCode());
	}

	@Test
	public void testHandleHttpMessageNotReadable() {
		Mockito.doReturn(new Exception()).when(httpMessageNotReadableExceptionMock).getMostSpecificCause();
		assertEquals(HttpStatus.BAD_REQUEST, baseExceptionHandlerMock
				.handleHttpMessageNotReadable(httpMessageNotReadableExceptionMock, null, null, null).getStatusCode());
	}

	@Test
	public void testHandleTypeMismatch() {
		assertEquals(HttpStatus.BAD_REQUEST, baseExceptionHandlerMock
				.handleTypeMismatch(typeMismatchExceptionMock, null, null, null).getStatusCode());
	}

	@Test
	public void testHandleHttpRequestMethodNotSupported() {
		assertEquals(HttpStatus.METHOD_NOT_ALLOWED, baseExceptionHandlerMock
				.handleHttpRequestMethodNotSupported(httpRequestMethodNotSupportedExceptionMock, null, null, null)
				.getStatusCode());
	}
}
*/