/*
 * Copyright (C) {year} VMware, LLC
 * https://VMware.com
 * helpnowplus-product-dev@vmware.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kroger.exception;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.kroger.exception.UserInActiveException;
import com.kroger.util.ServiceErrorCode;

public class UserInActiveExceptionTest {

	private UserInActiveException userInActiveException;

	private final String USER_INACTIVE_BAD_REQUEST_ERROR = "User Inactive Bad Request Exception";

	/**
	 * 
	 */
	@Test
	public void testGetCode() {
		userInActiveException = new UserInActiveException(USER_INACTIVE_BAD_REQUEST_ERROR,
				"User Inactive Bad Request Exception", ServiceErrorCode.USER_INACTIVE_ERROR);
		assertEquals(ServiceErrorCode.USER_INACTIVE_ERROR, userInActiveException.getCode());
	}

	/**
	 * 
	 */
	@Test
	public void testGetUserMessage() {
		userInActiveException = new UserInActiveException(USER_INACTIVE_BAD_REQUEST_ERROR,
				"User Inactive Bad Request Exception", ServiceErrorCode.USER_INACTIVE_ERROR);
		assertEquals("User Inactive Bad Request Exception", userInActiveException.getUserMessage());
	}

}
