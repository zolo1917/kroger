/*
 * Copyright (C) {year} VMware, LLC
 * https://VMware.com
 * helpnowplus-product-dev@vmware.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kroger.exception;

import com.kroger.util.ServiceErrorCode;

public class ConfigException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4352785473239919406L;

	private final ServiceErrorCode code;

	private final String message;

	/**
	 * @param message
	 * @param userMessage
	 * @param code
	 */
	public ConfigException(String message, String userMessage, ServiceErrorCode code) {
		super(message);
		this.code = code;
		this.message = userMessage;
	}

	public ServiceErrorCode getCode() {
		return this.code;
	}

	public String getUserMessage() {
		return this.message;
	}
}
