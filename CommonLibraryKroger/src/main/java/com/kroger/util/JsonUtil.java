/*
 * Copyright (C) {year} VMware, LLC
 * https://VMware.com
 * helpnowplus-product-dev@vmware.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kroger.util;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kroger.dto.UIErrorJSONDto;
import com.kroger.dto.UIErrorPayloadDto;

/**
 * @author HelpNowPlus-itdev@vmware.com
 *
 */
@Component
public class JsonUtil {

	private static final Logger _log = LoggerFactory.getLogger(JsonUtil.class);

	@Autowired
	@Qualifier("objectMapper")
	private ObjectMapper mapper;

	private static final String ERROR = "error";

	private static final String ERRORCODE = "errorCode";

	private static final String ERRORMESSAGE = "errorMessage";

	/**
	 * @param errorMessage
	 * @param errorCode
	 * @return
	 */
	public String buildErrorJson(String errorMessage, String errorCode) {
		String response = null;
		try {

			UIErrorPayloadDto uiErrorPayload = new UIErrorPayloadDto(errorMessage, errorCode);
			UIErrorJSONDto uiErrorJSON = new UIErrorJSONDto(uiErrorPayload);
			response = mapper.writeValueAsString(uiErrorJSON);
		} catch (JsonProcessingException e) {
			_log.error("Exception Error in creating Error Json", e.getMessage(), e);
		}

		return response;
	}

	/**
	 * @param errorMessage
	 * @param errorCode
	 * @return
	 */
	public String buildSuccessJson(String message) {
		String response = null;
		try {

			Map<String, Object> responseMap = new HashMap<>();
			responseMap.put("message", message);
			response = mapper.writeValueAsString(responseMap);
		} catch (JsonProcessingException e) {
			_log.error("Exception Error in creating Error Json", e.getMessage(), e);
		}

		return response;
	}

	/**
	 * @param errorMessage
	 * @param errorCode
	 * @return
	 */
	public Map<String, Object> buildErrorResponseMap(String errorMessage, String errorCode) {
		Map<String, Object> errorMsg = new HashMap<>();
		Map<String, Object> response = new HashMap<>();
		errorMsg.put(ERRORCODE, errorCode);
		errorMsg.put(ERRORMESSAGE, errorMessage);
		response.put(ERROR, errorMsg);
		return response;
	}

}
