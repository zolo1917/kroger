package com.kroger.util;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang3.StringUtils;

public class ErrorUtil {

	public static final String HYPHEN = "-";
	public static final String DOUBLECOLONN = "::";
	public static final String COLON = ":";
	public static final String SPACE = " ";

	/**
	 * private constructor.
	 */
	private ErrorUtil() {

	}

	/**
	 * Creates the error message.
	 *
	 * @param service    the service
	 * @param moduleName the module name
	 * @param className  the class name
	 * @param exception  the exception
	 * @return the string
	 */
	public static String createErrorMessage(String service, String moduleName, String className, Exception exception) {
		StringBuilder errorMessage = new StringBuilder();
		errorMessage.append(service).append(SPACE).append(moduleName).append(SPACE)
				.append(getErrorDetails(exception, className));
		return errorMessage.toString();
	}

	/**
	 * Gets the error details.
	 *
	 * @param exception the exception
	 * @param className the class name
	 * @return the error details
	 */
	public static String getErrorDetails(Exception exception, String className) {
		StringBuilder createErrorMessage = new StringBuilder();
		for (StackTraceElement stackTraceElement : exception.getStackTrace()) {
			if (StringUtils.endsWith(stackTraceElement.getClassName(), "." + className)) {
				createErrorMessage.append(stackTraceElement.getFileName()).append(" - ").append("Line no:")
						.append(stackTraceElement.getLineNumber()).append(" - ").append(" Exception : ")
						.append(exception).append(ExceptionUtils.getStackTrace(exception));
				break;
			}
		}
		return createErrorMessage.toString();
	}

	/**
	 * Creates the error message.
	 *
	 * @param service    the service
	 * @param moduleName the module name
	 * @param className  the class name
	 * @param message    the message
	 * @return the string
	 */
	public static String createErrorMessage(String service, String moduleName, String className, String message) {
		StringBuilder errorMessage = new StringBuilder();
		errorMessage.append(service).append(SPACE).append(moduleName).append(SPACE).append(className).append(SPACE)
				.append(message).append(SPACE);
		return errorMessage.toString();
	}

	/**
	 * Creates the error message.
	 *
	 * @param message the message
	 * @return the string
	 */
	public static String createErrorMessage(String message) {
		StringBuilder errorMessage = new StringBuilder();
		errorMessage.append(message).append(SPACE);
		return errorMessage.toString();
	}

}
