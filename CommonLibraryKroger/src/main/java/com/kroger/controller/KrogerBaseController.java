/*
 * Copyright (C) {year} VMware, LLC
 * https://VMware.com
 * helpnowplus-product-dev@vmware.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kroger.controller;

import static com.kroger.util.CommonConstants.ACCESS_TOKEN_INVALID_MSG;
import static com.kroger.util.CommonConstants.AUTHORITIES;
import static com.kroger.util.CommonConstants.BAD_REQUEST_ERROR_CODE;
import static com.kroger.util.CommonConstants.CLIENT_ID;
import static com.kroger.util.CommonConstants.UNAUTHORIZED_ACCESS_HTTP_CODE;
import static com.kroger.util.CommonConstants.USERNAME;
import static com.kroger.util.CommonConstants.USERNAME_PATTERN;
import static com.kroger.util.CommonConstants.USER_HAS_NO_EXPECTED_ROLE;
import static com.kroger.util.CommonConstants.USER_IS_NOT_OF_ROLE_USER;
import static com.kroger.util.CommonConstants.X_REMOTE_USER_INVALID_ERROR_MSG;
import static com.kroger.util.CommonConstants.X_REMOTE_USER_MANDATORY_ERROR_MSG;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.StringUtils;
import org.springframework.validation.DataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kroger.exception.KrogApplicationException;
import com.kroger.exception.UserUnauthorizedException;
import com.kroger.util.JWTTokenUtil;
import com.kroger.util.JsonUtil;
import com.kroger.util.ServiceErrorCode;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class KrogerBaseController {

	@Value("${spring.application.name}")
	public String appName;

	@Autowired
	@Qualifier("objectMapper")
	public ObjectMapper mapper;

	@Autowired
	@Qualifier("restTemplate")
	public RestTemplate restTemplate;

	@Autowired
	@Qualifier("jsonUtil")
	public JsonUtil jsonUtil;

	/*
	 * @Autowired
	 * 
	 * @Qualifier("serviceCacheMap") public Map<String, String> serviceCacheMap;
	 */

	@Autowired
	@Qualifier("jwtTokenUtil")
	public JWTTokenUtil jwtTokenUtil;

	@InitBinder
	public void activateDirectFieldAccess(DataBinder dataBinder) {
		dataBinder.initDirectFieldAccess();
	}

	/**
	 * @param authBearer
	 * @param xRemoteUser
	 * @param xImpersonateUser
	 * @return
	 * @throws KrogApplicationException
	 */
	public Boolean validateUserAccessNRoles(String authBearer, String xRemoteUser, String xImpersonateUser)
			throws KrogApplicationException {

		String usernameFromToken = null;
		String clientIdFromToken = null;
		boolean isValidUser = false;

		Map<String, Object> accessTokenMap = jwtTokenUtil.getJWTPayloadFromAccessToken(authBearer);

		if (accessTokenMap != null && (accessTokenMap.containsKey(USERNAME) && accessTokenMap.containsKey(CLIENT_ID))) {

			if (accessTokenMap.containsKey(USERNAME)) {
				usernameFromToken = (String) accessTokenMap.get(USERNAME);
			}

			if (accessTokenMap.containsKey(CLIENT_ID)) {
				clientIdFromToken = (String) accessTokenMap.get(CLIENT_ID);
			}

		} else {
			throw new KrogApplicationException(xRemoteUser + ACCESS_TOKEN_INVALID_MSG, BAD_REQUEST_ERROR_CODE,
					ServiceErrorCode.ACCESS_TOKEN_INVALID_ERROR.getErrorCode());
		}

		validateXRemoteUser(xRemoteUser);

		log.debug("Inside HNBaseController.validateUserAccessNRoles ---Clientid={}-->Started", clientIdFromToken);

		return validateLoginHeaders(authBearer, xRemoteUser, xImpersonateUser, usernameFromToken, isValidUser);
	}

	/**
	 * @param authBearer
	 * @param xRemoteUser
	 * @param xImpersonateUser
	 * @param usernameFromToken
	 * @param isValidUser
	 * @return
	 * @throws KrogApplicationException
	 */
	private boolean validateLoginHeaders(String authBearer, String xRemoteUser, String xImpersonateUser,
			String usernameFromToken, boolean isValidUser) throws KrogApplicationException {
		// Usual Case
		if (!StringUtils.isEmpty(xRemoteUser) && StringUtils.isEmpty(xImpersonateUser)) {

			log.debug("Inside HNBaseController.validateUserAccessNRoles --->Non-Impersonation Flow");

			if (xRemoteUser.equalsIgnoreCase(usernameFromToken)) {
				isValidUser = true;
			} else {
				throw new KrogApplicationException(xRemoteUser + ACCESS_TOKEN_INVALID_MSG, UNAUTHORIZED_ACCESS_HTTP_CODE,
						ServiceErrorCode.EMPTY_AUTHORITIES_ERROR.getErrorCode());
			}
			// Impersonation case
		} else if (!StringUtils.isEmpty(xRemoteUser) && !StringUtils.isEmpty(xImpersonateUser)) {

			if (xRemoteUser.equalsIgnoreCase(xImpersonateUser)) {
				throw new KrogApplicationException("xRemoteUser and xImpersonateUser header values cannot be same", "400",
						ServiceErrorCode.XREMOTEUSER_XIMPERSONATE_HEADERS_SAME_ERROR.getErrorCode());
			}

			log.debug("Inside HNBaseController.validateUserNToken ---->Impersonation Flow -->started");

			if (validateImpersonateRoleAndAccessToken(xImpersonateUser, authBearer)) {
				log.debug(
						"Inside HNBaseController.validateUserNToken --->Impersonation Flow -->{} has Impersonator Role & Access Token is matched with the user ",
						xImpersonateUser);
				isValidUser = true;
			}
		}
		return isValidUser;
	}

	/**
	 * @param xRemoteUser
	 * @throws KrogApplicationException
	 */
	private void validateXRemoteUser(String xRemoteUser) throws KrogApplicationException {
		if (StringUtils.isEmpty(xRemoteUser)) {
			throw new KrogApplicationException(X_REMOTE_USER_MANDATORY_ERROR_MSG, BAD_REQUEST_ERROR_CODE,
					ServiceErrorCode.XREMOTEUSER_EMPTY_ERROR.toString());
		} else if (!USERNAME_PATTERN.matcher(xRemoteUser).matches()) {
			throw new KrogApplicationException(X_REMOTE_USER_INVALID_ERROR_MSG, BAD_REQUEST_ERROR_CODE,
					ServiceErrorCode.XREMOTEUSER_EMPTY_ERROR.getErrorCode());
		}
	}

	/**
	 * @param xImpersonateUser
	 * @param authBearer
	 * @return
	 * @throws KrogApplicationException
	 */
	@SuppressWarnings("unchecked")
	private Boolean validateImpersonateRoleAndAccessToken(String xImpersonateUser, String authBearer)
			throws KrogApplicationException {

		Map<String, Object> accessTokenMap = jwtTokenUtil.getJWTPayloadFromAccessToken(authBearer);

		if (jwtTokenUtil.validateUserWithAccessToken(accessTokenMap, xImpersonateUser)) {

			if (accessTokenMap.containsKey(AUTHORITIES)) {
				List<String> userRoles = (List<String>) accessTokenMap.get(AUTHORITIES);

				if (userRoles != null && !userRoles.isEmpty()) {

					if (userRoles.contains("Impersonator")) {
						return true;
					} else {
						throw new KrogApplicationException(xImpersonateUser + " does have Impersonator Role",
								BAD_REQUEST_ERROR_CODE, ServiceErrorCode.IMPERSONATION_ERROR.getErrorCode());
					}

				} else {
					throw new KrogApplicationException("Access Token is invalid , it has no Roles",
							BAD_REQUEST_ERROR_CODE, ServiceErrorCode.EMPTY_ROLES_ERROR.getErrorCode());
				}

			} else {
				throw new KrogApplicationException("Access Token is invalid , it has no Authorities",
						BAD_REQUEST_ERROR_CODE, ServiceErrorCode.EMPTY_AUTHORITIES_ERROR.getErrorCode());
			}

		} else {
			throw new KrogApplicationException(xImpersonateUser + "'s access token has not matched with xImpersonateUser",
					BAD_REQUEST_ERROR_CODE, ServiceErrorCode.ACCESS_TOKEN_INVALID_ERROR.getErrorCode());
		}
	}

	/**
	 * @param authBearer
	 * @param xRemoteUser
	 * @param xImpersonateUser
	 * @param expectedRoles
	 * @return
	 */
	protected boolean validateUserRole(String authBearer, String xRemoteUser, String xImpersonateUser,
			String[] expectedRoles) {
		Map<String, Object> accessTokenMap = jwtTokenUtil.getJWTPayloadFromAccessToken(authBearer);
		boolean isRoleExists = false;
		List<String> expectedRolesList = Arrays.asList(expectedRoles);
		String user = getUserHeader(xRemoteUser, xImpersonateUser);
		return validateAccessTokenAuthorities(expectedRoles, accessTokenMap, isRoleExists, expectedRolesList, user);
	}

	/**
	 * @param expectedRoles
	 * @param accessTokenMap
	 * @param isRoleExists
	 * @param expectedRolesList
	 * @param user
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private boolean validateAccessTokenAuthorities(String[] expectedRoles, Map<String, Object> accessTokenMap,
			boolean isRoleExists, List<String> expectedRolesList, String user) {
		if (accessTokenMap.containsKey(AUTHORITIES)) {
			List<String> userRoles = (List<String>) accessTokenMap.get(AUTHORITIES);

			isRoleExists = validateUserExpectedRoles(expectedRoles, isRoleExists, expectedRolesList, user, userRoles);

		} else {
			throw new UserUnauthorizedException(String.format(USER_IS_NOT_OF_ROLE_USER, expectedRolesList),
					String.format("%s is not having proper jwt authorities", user),
					ServiceErrorCode.USER_HAS_NO_EXPECTED_ROLE);
		}
		return isRoleExists;
	}

	/**
	 * @param expectedRoles
	 * @param isRoleExists
	 * @param expectedRolesList
	 * @param user
	 * @param userRoles
	 * @return
	 */
	private boolean validateUserExpectedRoles(String[] expectedRoles, boolean isRoleExists,
			List<String> expectedRolesList, String user, List<String> userRoles) {
		if (userRoles != null && !userRoles.isEmpty()) {

			for (int i = 0; i < expectedRoles.length; i++) {
				if (userRoles.contains(expectedRoles[i])) {
					isRoleExists = true;
				}
			}

			if (!isRoleExists) {
				throw new UserUnauthorizedException(String.format(USER_IS_NOT_OF_ROLE_USER, expectedRolesList),
						String.format(USER_HAS_NO_EXPECTED_ROLE, user, expectedRolesList),
						ServiceErrorCode.USER_HAS_NO_EXPECTED_ROLE);
			}

		} else {
			throw new UserUnauthorizedException(String.format(USER_IS_NOT_OF_ROLE_USER, expectedRolesList),
					String.format("%s is not having proper roles", user), ServiceErrorCode.USER_HAS_NO_EXPECTED_ROLE);
		}
		return isRoleExists;
	}

	/**
	 * @param xRemoteUser
	 * @param xImpersonateUser
	 * @return
	 */
	private String getUserHeader(String xRemoteUser, String xImpersonateUser) {
		String user = null;

		if (!StringUtils.isEmpty(xRemoteUser) && StringUtils.isEmpty(xImpersonateUser)) {
			user = xRemoteUser;
		}

		if (!StringUtils.isEmpty(xRemoteUser) && !StringUtils.isEmpty(xImpersonateUser)) {
			user = xImpersonateUser;
		}
		return user;
	}

}
