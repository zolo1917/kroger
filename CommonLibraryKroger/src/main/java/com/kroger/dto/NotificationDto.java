/*
 * Copyright (C) {year} VMware, LLC
 * https://VMware.com
 * helpnowplus-product-dev@vmware.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * Copyright (C) {year} VMware, LLC
 * https://VMware.com
 * helpnowplus-product-dev@vmware.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kroger.dto;

import java.io.Serializable;

import lombok.Data;

/**
 * The Class NotificationDto.
 */
@Data
public class NotificationDto implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2316376043382270173L;

	/** The ticket id. */
	private String ticketId;

	/** The notification type. */
	private String notificationType;

	/** The notification status id. */
	private Long notificationStatusId;

	/** The comment id. */
	private Long commentId;

	/** The task id. */
	private String taskId;

	/** The ticket item id. */
	private String ticketItemId;

	/** The kafka type. */
	private String kafkaType;

	/** The catalog id. */
	private String catalogId;

	/** The attachment id. */
	private String attachmentId;

	/** The approval id. */
	private String approvalId;

	/**
	 * Instantiates a new notification dto.
	 */
	public NotificationDto() {

	}

	/**
	 * Instantiates a new notification dto.
	 *
	 * @param ticketId         the ticket id
	 * @param notificationType the notification type
	 */
	public NotificationDto(String ticketId, String notificationType) {
		this.ticketId = ticketId;
		this.notificationType = notificationType;
	}

}
