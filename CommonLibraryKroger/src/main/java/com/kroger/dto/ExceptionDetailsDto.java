package com.kroger.dto;

import java.io.Serializable;

import org.springframework.http.HttpHeaders;

import lombok.Data;

/**
 * The Class ExceptionDetailsDto.
 */
@Data
public class ExceptionDetailsDto implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4673114991079283229L;

	/** The service name. */
	private String serviceName;

	/** The httpHeaders. */
	private HttpHeaders requestHttpHeaders;

	/** The exception. */
	private Exception exception;

	/** The request URL. */
	private String requestURL;

	/** The request body. */
	private String requestBody;

	/** The response body. */
	private String responseBody;

}
