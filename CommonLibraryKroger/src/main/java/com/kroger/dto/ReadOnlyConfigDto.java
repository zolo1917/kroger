/*
 * Copyright (C) {year} VMware, LLC
 * https://VMware.com
 * helpnowplus-product-dev@vmware.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kroger.dto;

import java.io.Serializable;

import lombok.Data;

/**
 * The Class ReadOnlyConfigDto.
 */
@Data
public class ReadOnlyConfigDto implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -894598180764621217L;

	/** The id. */
	private Long id;

	/** The services. */
	private String services;

	/** The message. */
	private String message;

	/** The created by. */
	private String createdBy;

	/** The readonly. */
	private String readonly;

	/**
	 * Instantiates a new read only config dto.
	 */
	public ReadOnlyConfigDto() {
		super();
	}

	/**
	 * Instantiates a new read only config dto.
	 *
	 * @param id       the id
	 * @param services the services
	 * @param message  the message
	 * @param readonly the readonly
	 */
	public ReadOnlyConfigDto(Long id, String services, String message, String readonly) {
		super();
		this.id = id;
		this.services = services;
		this.message = message;
		this.readonly = readonly;
	}

	/**
	 * Instantiates a new read only config dto.
	 *
	 * @param services  the services
	 * @param message   the message
	 * @param createdBy the created by
	 * @param readonly  the readonly
	 */
	public ReadOnlyConfigDto(String services, String message, String createdBy, String readonly) {
		super();
		this.services = services;
		this.message = message;
		this.createdBy = createdBy;
		this.readonly = readonly;
	}

}
