/*
 * Copyright (C) {year} VMware, LLC
 * https://VMware.com
 * helpnowplus-product-dev@vmware.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kroger.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;

/**
 * The Class SearchRequestDto.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class SearchRequestDto implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -3012056452953348891L;

	/** The query param. */
	private String queryParam = null;

	/** The active. */
	private String active = null;

	/** The max results. */
	private String maxResults = null;

	/** The start at. */
	private String startAt = null;

	/** The fields. */
	private String fields = null;

	/** The order by. */
	private String orderBy = null;

	/** The type. */
	private String type = null;

	/** The api. */
	private String api = null;

	/** The sap payload. */
	private String sapPayload = null;

	/** The sap request URL. */
	private String sapRequestURL = null;

	/** The url path variables. */
	private Map<String, String> urlPathVariables = new HashMap<>();

	/**
	 * <p>
	 * <ul>
	 * <li>For LocationService & UserService</li>
	 * <li>For Group Service , will pass active as null.</li>
	 * </ul>
	 *
	 * @param queryParam    the query param
	 * @param active        the active
	 * @param maxResults    the max results
	 * @param startAt       the start at
	 * @param fields        the fields
	 * @param orderBy       the order by
	 * @param type          the type
	 * @param api           - Broker Service API
	 * @param sapPayload    - SAP Payload received
	 * @param sapRequestURL - SAP EndPoint URL
	 */
	public SearchRequestDto(String queryParam, String active, String maxResults, String startAt, String fields,
			String orderBy, String type, String api, String sapPayload, String sapRequestURL) {
		super();
		this.queryParam = queryParam;
		this.active = active;
		this.maxResults = maxResults;
		this.startAt = startAt;
		this.fields = fields;
		this.orderBy = orderBy;
		this.type = type;
		this.api = api;
		this.sapPayload = sapPayload;
		this.sapRequestURL = sapRequestURL;
	}

	/**
	 * <p>
	 * <ul>
	 * <li>For LocationService & UserService</li>
	 * <li>For Group Service , will pass active as null.</li>
	 * </ul>
	 *
	 * @param queryParam       the query param
	 * @param active           the active
	 * @param maxResults       the max results
	 * @param startAt          the start at
	 * @param fields           the fields
	 * @param orderBy          the order by
	 * @param type             the type
	 * @param api              the api
	 * @param sapPayload       the sap payload
	 * @param sapRequestURL    the sap request URL
	 * @param urlPathVariables the url path variables
	 */
	public SearchRequestDto(String queryParam, String active, String maxResults, String startAt, String fields,
			String orderBy, String type, String api, String sapPayload, String sapRequestURL,
			Map<String, String> urlPathVariables) {
		super();
		this.queryParam = queryParam;
		this.active = active;
		this.maxResults = maxResults;
		this.startAt = startAt;
		this.fields = fields;
		this.orderBy = orderBy;
		this.type = type;
		this.api = api;
		this.sapPayload = sapPayload;
		this.sapRequestURL = sapRequestURL;
		this.urlPathVariables = urlPathVariables;
	}

	/**
	 * Instantiates a new search request dto.
	 *
	 * @param sapRequestURL the sap request URL
	 */
	public SearchRequestDto(String sapRequestURL) {
		super();
		this.sapRequestURL = sapRequestURL;
	}

}
