/*
 * Copyright (C) {year} VMware, LLC
 * https://VMware.com
 * helpnowplus-product-dev@vmware.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kroger.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

/**
 * The Class UIErrorJSONDto.
 *
 * @author HelpNowPlus-itdev@vmware.com
 */
@JsonAutoDetect(fieldVisibility = Visibility.NONE, getterVisibility = Visibility.PUBLIC_ONLY)
@Data
public class UIErrorJSONDto {
	
	/** The ui error payload. */
	private UIErrorPayloadDto uiErrorPayload;
	
	/**
	 * Instantiates a new UI error JSON dto.
	 *
	 * @param uiErrorPayload the ui error payload
	 */
	@JsonCreator
	public UIErrorJSONDto(@JsonProperty("uiErrorPayload") UIErrorPayloadDto uiErrorPayload){
		this.uiErrorPayload = uiErrorPayload;
	}

	/**
	 * Gets the ui error payload.
	 *
	 * @return the ui error payload
	 */
	@JsonProperty("error")
	public UIErrorPayloadDto getUiErrorPayload() {
		return uiErrorPayload;
	}

}
