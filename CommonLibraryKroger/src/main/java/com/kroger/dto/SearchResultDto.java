/*
 * Copyright (C) {year} VMware, LLC
 * https://VMware.com
 * helpnowplus-product-dev@vmware.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kroger.dto;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * The Class SearchResultDto.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "maxResults", "startAt", "total", "results" })
@Getter
@Setter
@ToString
@NoArgsConstructor
public class SearchResultDto {

	/** The max results. */
	@JsonProperty("maxResults")
	private int maxResults;

	/** The start at. */
	@JsonProperty("startAt")
	private int startAt;

	/** The total. */
	@JsonProperty("total")
	private Long total;

	/** The results. */
	@JsonProperty("results")
	private List<? extends Object> results = new ArrayList<>();

	/**
	 * Instantiates a new search result dto.
	 *
	 * @param maxResults the max results
	 * @param startAt the start at
	 * @param total the total
	 * @param results the results
	 */
	public SearchResultDto(int maxResults, int startAt, Long total, List<? extends Object> results) {
		super();
		this.maxResults = maxResults;
		this.startAt = startAt;
		this.total = total;
		this.results = results;
	}

}
