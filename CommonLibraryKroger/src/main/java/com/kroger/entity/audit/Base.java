/*
 * Copyright (C) {year} VMware, LLC
 * https://VMware.com
 * kumaravinas@vmware.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 

package com.kroger.entity.audit;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.format.annotation.DateTimeFormat;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

*//**
 * The Class Base.
 *//*
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@Getter
@Setter
@ToString
public abstract class Base implements Serializable {

	*//** The Constant serialVersionUID. *//*
	private static final long serialVersionUID = -5603369217549852033L;

	*//** The created on. *//*
	@CreatedDate
	@Column(name = "created_on")
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
	private Date createdOn;

	*//** The created by. *//*
	@CreatedBy
	@Column(name = "created_by", nullable = false)
	private Long createdBy;

	*//** The updated on. *//*
	@LastModifiedDate
	@Column(name = "updated_on")
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
	private Date updatedOn;

	*//** The updated by. *//*
	@LastModifiedBy
	@Column(name = "updated_by")
	private Long updatedBy;

	*//** The x impersonate user. *//*
	private transient String xImpersonateUser;

}
*/