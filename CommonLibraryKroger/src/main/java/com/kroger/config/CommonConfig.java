/*
 * Copyright (C) {year} VMware, LLC
 * https://VMware.com
 * helpnowplus-product-dev@vmware.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kroger.config;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ITemplateResolver;
import org.thymeleaf.templateresolver.StringTemplateResolver;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.kroger.logging.LoggingAspect;
import com.kroger.util.CommonConstants;

/**
 * The Class CommonConfig.
 *
 * @author HelpNowPlus-itdev@vmware.com
 */
@Configuration
public class CommonConfig {

	/**
	 * Object mapper.
	 *
	 * @return the object mapper
	 */
	@Bean(name = "objectMapper")
	public ObjectMapper objectMapper() {
		ObjectMapper mapper = new ObjectMapper();
		mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
		return mapper;
	}

	/**
	 * Rest template.
	 *
	 * @return the rest template
	 */
	@Bean(name = "restTemplate")
	public RestTemplate restTemplate() {
		List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
		messageConverters.add(new StringHttpMessageConverter(Charset.forName("UTF-8")));
		messageConverters.add(new MappingJackson2HttpMessageConverter(objectMapper()));

		RestTemplate restTemplate = new RestTemplate(clientHttpRequestFactory());
		restTemplate.setMessageConverters(messageConverters);

		return restTemplate;
	}

	/**
	 * Client http request factory.
	 *
	 * @return the client http request factory
	 */
	// Setting timeout of 30 secs
	private ClientHttpRequestFactory clientHttpRequestFactory() {
		HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
		factory.setReadTimeout(3000000);
		factory.setConnectTimeout(3000000);
		return factory;
	}

	/**
	 * Bean for logging.
	 *
	 * @param env the env
	 * @return the logging aspect
	 */
	@Bean
	@Profile(CommonConstants.SPRING_PROFILE_DEVELOPMENT)
	public LoggingAspect loggingAspect(Environment env) {
		return new LoggingAspect(env);
	}

	/**
	 * Spring template engine.
	 *
	 * @return the spring template engine
	 */
	@Bean
	public SpringTemplateEngine springTemplateEngine() {
		SpringTemplateEngine templateEngine = new SpringTemplateEngine();
		templateEngine.addTemplateResolver(stringTemplateResolver());
		return templateEngine;
	}

	/**
	 * String template resolver.
	 *
	 * @return the i template resolver
	 */
	@Bean
	public ITemplateResolver stringTemplateResolver() {
		final StringTemplateResolver templateResolver = new StringTemplateResolver();
		templateResolver.setOrder(Integer.valueOf(3));
		templateResolver.setTemplateMode(TemplateMode.HTML);
		templateResolver.setCacheable(false);
		return templateResolver;
	}

}
