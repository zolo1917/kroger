/*
 * Copyright (C) {year} VMware, LLC
 * https://VMware.com
 * helpnowplus-product-dev@vmware.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kroger.service;

import java.io.IOException;
import java.util.Map;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kroger.exception.BadDataException;
import com.kroger.util.CommonBeanUtils;
import com.kroger.util.CommonConstants;
import com.kroger.util.ServiceErrorCode;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class BaseService {

	public static final Pattern NUMBER_PATTERN = Pattern.compile("(\\d+)?");

	private static final String ERROR_KEY = "error";

	private static final String MESSAGE_KEY = "message";

	private static final String VALUE_KEY = "value";

	private static final String ERROR_MESSAGE_KEY = "errorMessage";

	@Autowired
	@Qualifier("objectMapper")
	public ObjectMapper mapper;

	@Autowired
	@Qualifier("serviceCacheMap")
	public Map<String, String> serviceCacheMap;

	@Autowired
	public CommonBeanUtils commonBeanUtils;


	/** The error notifier email recipients cache. */
	@Autowired
	@Qualifier("errorNotifierEmailRecipientsCache")
	public Map<String, String> errorNotifierEmailRecipientsCache;

	/** The api url cache. */
	@Autowired
	@Qualifier("apiUrlCache")
	public Map<String, String> apiUrlCache;

	/**
	 * @param authBearer
	 * @param xRemoteUser
	 * @param xImpersonateUser
	 * @return
	 */
	public HttpHeaders addRequiredHeadersForBS(String authBearer, String xRemoteUser, String xImpersonateUser) {

		HttpHeaders headers = new HttpHeaders();
		headers.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
		headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);

		if (!StringUtils.isEmpty(authBearer)) {
			headers.add(CommonConstants.AUTHORIZATION_HEADER, authBearer);
		}

		if (!StringUtils.isEmpty(xRemoteUser)) {
			headers.add(CommonConstants.XREMOTE_USER_HEADER, xRemoteUser);
		}

		if (!StringUtils.isEmpty(xImpersonateUser)) {
			headers.add(CommonConstants.XIMPERSONATE_USER_HEADER, xImpersonateUser);
		}
		return headers;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public String filterErrorValue(String error) {
		String errorMessage = null;
		Map<String, Object> sapErrorMessage = null;
		try {
			sapErrorMessage = mapper.readValue(error, new TypeReference<Map<String, Object>>() {
			});
			if (((Map) sapErrorMessage.get(ERROR_KEY)).containsKey(ERROR_MESSAGE_KEY)) {
				errorMessage = ((Map) sapErrorMessage.get(ERROR_KEY)).get(ERROR_MESSAGE_KEY).toString();
				return errorMessage;
			}
			Map<String, Object> errorMap = (Map<String, Object>) sapErrorMessage.get(ERROR_KEY);
			Map<String, Object> message = (Map<String, Object>) errorMap.get(MESSAGE_KEY);
			errorMessage = (String) message.get(VALUE_KEY);
		} catch (IOException e) {
			log.error("Error in filtering error response body..{}", e.getMessage());
		}
		return errorMessage;
	}

	/**
	 * @param value
	 * @return
	 */
	public String validateBooleanParameter(String value, String parameterName) {
		if (CommonConstants.ALL.equals(value)) {
			return org.apache.commons.lang.StringUtils.EMPTY;
		} else if (String.valueOf(Boolean.TRUE).equals(value)) {
			return Integer.toString(1);
		} else if (String.valueOf(Boolean.FALSE).equals(value)) {
			return Integer.toString(0);
		}
		throw new BadDataException(
				String.format(CommonConstants.INVALID_BOOLEAN_PARAMETER_VALUE_MESSAGE, parameterName),
				CommonConstants.DATA_VALIDATION_FAILED, ServiceErrorCode.INVALID_BOOLEAN_PARAMETER_VALUE_ERROR);
	}

	/**
	 * @param text
	 * @param parameterName
	 * @return
	 */
	public boolean validateIntegerParameter(String text, String parameterName) {
		if (text == null)
			return false;
		if (!commonBeanUtils.isNumeric(text) || Math.signum(Double.parseDouble(text)) < 0) {
			throw new BadDataException(
					String.format(CommonConstants.INVALID_INTEGER_PARAMETER_VALUE_MESSAGE, parameterName),
					CommonConstants.DATA_VALIDATION_FAILED, ServiceErrorCode.INVALID_INTEGER_PARAMETER_VALUE_ERROR);
		}
		return true;
	}

	/**
	 * @param queryParams
	 * @param offset
	 * @return
	 */
	public int validateNPopulateOffset(Map<String, String> queryParams, int offset) {
		if (validateIntegerParameter(queryParams.get(CommonConstants.START_AT), CommonConstants.START_AT)) {
			offset = Integer.parseInt(queryParams.get(CommonConstants.START_AT));
		}
		return offset;
	}
	
	/**
	 * @param queryParams
	 * @param from
	 * @return
	 */
	public int validateNPopulateFrom(Map<String, String> queryParams, int from) {
		if (validateIntegerParameter(queryParams.get(CommonConstants.FROM), CommonConstants.FROM)) {
			from = Integer.parseInt(queryParams.get(CommonConstants.FROM));
		}
		return from;
	}

	/**
	 * @param queryParams
	 * @param pageSize
	 * @return
	 */
	public int validateNPopulatePageSize(Map<String, String> queryParams, int pageSize) {
		if (validateIntegerParameter(queryParams.get(CommonConstants.MAX_RESULTS), CommonConstants.MAX_RESULTS)) {
			pageSize = Integer.parseInt(queryParams.get(CommonConstants.MAX_RESULTS));
		}
		return pageSize;
	}


	/**
	 * @param queryParams
	 * @param pageSize
	 * @return
	 */
	public int validateNPopulateSearchSize(Map<String, String> queryParams, int pageSize) {
		if (validateIntegerParameter(queryParams.get(CommonConstants.SIZE), CommonConstants.SIZE)) {
			pageSize = Integer.parseInt(queryParams.get(CommonConstants.SIZE));
		}
		return pageSize;
	}

	
	/**
	 * @param orderByFieldName
	 * @return
	 */
	public String validateNPopulateDefaultOrderByFieldName(String orderByFieldName, String defaultField) {
		if (org.apache.commons.lang.StringUtils.isBlank(orderByFieldName)) {
			orderByFieldName = defaultField;
		}
		return orderByFieldName;
	}

	/**
	 * @param queryParams
	 * @return
	 */
	public String validateNPopulateDefaultSearchTerm(Map<String, String> queryParams) {
		return queryParams.get("q") != null ? queryParams.get("q") : org.apache.commons.lang.StringUtils.EMPTY;
	}

	/**
	 * @param queryParams
	 * @return
	 */
	public String validateNPopulateActive(Map<String, String> queryParams) {
		String active = Integer.toString(1);
		if (queryParams.get(CommonConstants.ACTIVE) != null) {
			active = validateBooleanParameter(queryParams.get(CommonConstants.ACTIVE), CommonConstants.ACTIVE);
		}
		return active;
	}

	/**
	 * @param pageSize
	 * @param page
	 * @param offset
	 * @return
	 */
	public int validateNPopulatePage(int pageSize, int page, int offset) {
		if (pageSize > 0) {
			page = offset / pageSize;
		}
		return page;
	}

	/**
	 * @param sortingOrder
	 * @return
	 *//*
	public Direction validateSortingOrder(String sortingOrder) {
		if (StringUtils.isEmpty(sortingOrder))
			return Sort.Direction.ASC;
		if (CommonConstants.ORDER_ASC.equalsIgnoreCase(sortingOrder))
			return Sort.Direction.ASC;
		else if (CommonConstants.ORDER_DESC.equalsIgnoreCase(sortingOrder))
			return Sort.Direction.DESC;
		throw new BadDataException(CommonConstants.INVALID_SORTING_ORDER_MESSAGE,
				CommonConstants.DATA_VALIDATION_FAILED, ServiceErrorCode.INVALID_SORTING_ORDER_ERROR);
	}*/

}
