package com.kroger.service;

import java.util.List;

import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kroger.exception.UserNotFoundException;
import com.kroger.model.ProdByCat;
import com.kroger.model.Products;
import com.kroger.model.User;
import com.kroger.repository.KrogerCatRepo;
import com.kroger.repository.KrogerProdRepo;
import com.kroger.repository.KrogerUserRepo;
import com.kroger.response.UserResponse;
import com.kroger.util.CommonConstants;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class KrogerService {

	@Autowired
	KrogerUserRepo krogerUserRepo;

	@Autowired
	KrogerCatRepo krogerCatRepo;

	@Autowired
	KrogerProdRepo krogerProdRepo;

	@Autowired
	UserResponse userResponse;

	public List<User> findAllUser() {
		
		log.info("Inside find all service");
		
		return (List<User>) krogerUserRepo.findAll();

	}

	public User findByUserId(String userId) {
		
		return krogerUserRepo.findById(userId)
				.orElseThrow(() -> new UserNotFoundException("User not found for an id : "+userId));
	}

	public List<User> findByUserType(String userType) {

		log.info("Insdie find by Type service");

		List<User> user = krogerUserRepo.findUserByType(userType);
		if (user.isEmpty())
			throw new UserNotFoundException("Users not found for a type : "+userType);
	
		return user;
		
	}

	public UserResponse saveUser(User user) {
		
		log.info("Inside save user service");
		
		krogerUserRepo.save(user);
		userResponse.setStatusCode(HttpStatus.SC_ACCEPTED);
		userResponse.setStatusMsg(CommonConstants.SUCCESS);
		userResponse.setResponse("User id : " + user.getUserId() + " inserted successfully");
		
		return userResponse;

	}

	public UserResponse deleteUser(String userId) {
		
		log.info("Inside delete by id service");

		if (krogerUserRepo.existsById(userId)) {
				krogerUserRepo.deleteById(userId);
				userResponse.setStatusCode(HttpStatus.SC_ACCEPTED);
				userResponse.setStatusMsg(CommonConstants.SUCCESS);
				userResponse.setResponse("User id : " + userId + " deleted successfully");
		 } else {
			 throw new UserNotFoundException("User not found for an id : "+userId);
			}
		return userResponse;
	}

	public UserResponse updateUser(String userId, String type) {
		log.info("Inside update User type by id");
		if (krogerUserRepo.existsById(userId)) {
				krogerUserRepo.updateUserTypeById(type, userId);
				userResponse.setStatusCode(HttpStatus.SC_ACCEPTED);
				userResponse.setStatusMsg(CommonConstants.SUCCESS);
				userResponse.setResponse("User id : " + userId + " Updated successfully");
		} else {
			throw new UserNotFoundException("User not found for an id : "+userId);
		}

		return userResponse;
	}

	public List<ProdByCat> findByCatName(String catName) {
		log.info("Inside find by cat name service");
		return krogerCatRepo.findByCatName(catName);
	}

	public ProdByCat findByCatNameAndId(String catName, String catId) {
		log.info("Inside find by cat name service");
		return krogerCatRepo.findByCatNameAndCatId(catName, catId);
	}

	public List<Products> findByProdName(String prodName) {
		log.info("Inside find by prod name Service");
		return krogerProdRepo.findByProdName(prodName);

	}

	public Products findByProdDesc(String prodDesc) {
		log.info("Inside find by price service");
		return krogerProdRepo.findByProdDesc(prodDesc);
	}

}
