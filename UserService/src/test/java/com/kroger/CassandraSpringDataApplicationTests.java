package com.kroger;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.kroger.controller.KrogerController;
import com.kroger.exception.UserNotFoundException;
import com.kroger.model.Products;
import com.kroger.model.User;
import com.kroger.repository.KrogerProdRepo;
import com.kroger.response.UserResponse;
import com.kroger.service.KrogerService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CassandraSpringDataApplicationTests {

	
	@Autowired
	KrogerController krogerController;
	
	@InjectMocks
	KrogerService krogerService;
	
	@MockBean
	KrogerProdRepo krogerProdRepo;
	
	@Autowired
	Products products;
	
	@Autowired
	User user;
	
	@Autowired
	User userres;
	
	
	@Before
	public void setup(){
	    MockitoAnnotations.initMocks(this); 
	}
	
	@Before
	public void userSetUp() {
		
		user.setFirstName("Rahul");
		user.setLastName("Kumar");
		user.setType("Prime");
		user.setUserId("U6");
	}
	
	@Before
	public void prodSetUp() {
		
		products.setProdId("M3");
		products.setProdDesc("IphoneXe");
		products.setProdName("Mobile");
		products.setProdPrice(20000);
	}
	
	@Test
	public void findByUserIdTest() {
		
		User user =krogerController.findByUserId("U1");
		assertEquals("Praveen",user.getFirstName());
	}
	
	@Test(expected = UserNotFoundException.class)
	public void findByUserIdExceptionTest() {
	
		krogerController.findByUserId("U100");
		
	}
	
	@Test
	public void saveUserTest() {
		
		UserResponse userResponse=krogerController.saveUser(user);
		
		assertEquals(HttpStatus.SC_ACCEPTED, userResponse.getStatusCode());
		assertEquals("Success", userResponse.getStatusMsg());
		assertEquals("User id : U6 inserted successfully", userResponse.getResponse());
	}
	
	@Test
	public void deleteUserTest() {
		
		UserResponse userResponse =krogerController.deleteUser("U6");
		assertEquals(202, userResponse.getStatusCode());
		assertEquals("Success", userResponse.getStatusMsg());
		assertEquals("User id : U6 deleted successfully", userResponse.getResponse());
	}
	
	@Test(expected = UserNotFoundException.class)
	public void deleteUserExceptionTest() {
		
		UserResponse userResponseRes =krogerController.deleteUser("U600");
		
	}
	
	  @Test 
	  public void updateUserTest() {
	  
		  krogerController.saveUser(user); 
		  UserResponse userResponse=krogerController.updateUser("U6","Non-Prime");
		  assertEquals(HttpStatus.SC_ACCEPTED,userResponse.getStatusCode());
		  assertEquals("Success", userResponse.getStatusMsg());
		  assertEquals("User id : U6 Updated successfully", userResponse.getResponse());
	 }
	  
	  @Test(expected = UserNotFoundException.class)
	  public void updateUserExceptionTest() {
		  
		  krogerController.deleteUser("U600");
		
	  }
	
	@Test
	public void findByMockTest() {
		krogerProdRepo = mock(KrogerProdRepo.class);
		when(krogerProdRepo.findByProdDesc("IphoneXe")).thenReturn(products);
		
		assertEquals("Mobile",krogerProdRepo.findByProdDesc("IphoneXe").getProdName());
		
	}
	
}
